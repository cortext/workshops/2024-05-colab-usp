# Oficina Cortext e Colab-USP 2024-05-22

Bem vindes à oficina [Cortext](https://www.cortext.net/) organizada pelo [Colab-USP](https://colab.each.usp.br/)!

[Página na Wikiversidade](https://pt.wikiversity.org/wiki/Utilizador:Solstag/Rol%C3%AAs_de_maio_2024)

## Programa

Aspectos teóricos e práticos alternar-se-ão nos períodos da manhã e da tarde.

### Manhã
Teórico: processamento de linguagem natural; da morfosintaxe à sociologia da tradução.

Prático: carregamento dos dados e trabalho com o texto.

### Tarde
Teórico: análise de redes e inferência estatística; visualizações e dialética humano-computacional.

Prático: produção de mapas e sua interpretação.

Tendo tempo, uma rápida apresentação dos resultados de alguns participantes.

## Instruções para participantes

Leia aqui como se preparar para nossa jornada.

### Material (caso 1): Você já tem dados com os quais deseja trabalhar?

Quem já tem dados pode prepará-los trazendo-os tanto que possível:
- Formatados em CSV
  - Por exemplo, de uma tabela do Calc ou Excel via 'exportar' ou 'salvar como'
- Com codificação UTF-8
- Estruturados de forma que:
  - Cada linha corresponda a um documento
  - E as colunas aos seus dados textuais e outros metadados
  - Se as células de certas colunas devem conter listas de elementos, usar um mesmo separador que não apareça em nenhum lugar do arquivo, mesmo em outras colunas

#### Exemplo

O tema e as colunas deste exemplo são meramente ilustrativas. Não há limite mínimo nem máximo de comprimento para as células de texto, mas é preciso considerar a unidade de texto que se deseja analisar. O "documento" – isto é, cada linha da tabela – pode corresponder a uma mensagem, um parágrafo, um resumo, uma cena, uma seção etc.

Aqui usamos o texto `&sep;` como separador para células com listas. Não há limites para o número de items numa lista.

| id | texto                          | título           | ano  | personagens                                |
|----|--------------------------------|------------------|------|--------------------------------------------|
| 1  | … perder-se também é caminho … | A cidade sitiada | 1949 | Lucrécia &sep; Felipe &sep; Mateus &sep; … |
| 2  | … ao vencedor, as batatas …    | Quincas Borba    | 1892 | Pedro &sep; Quincas &sep; Sofia &sep; …    |


### Material (caso 2): Você busca um conjunto de dados com o qual praticar?

Quem ainda não tem dados pode pensar em um tema e:
- Formualar palavras chave que o delimitem
- Realizar uma busca por elas na base OpenAlex
  - https://openalex.org/
- Se for o caso, refinar a escolha usando outros critérios como países ou anos
- Modificar o conjunto de palavras chave e critérios até obter uma busca:
  - Coerente nos resultados obtidos
  - Com entre 1 mil e 10 mil documentos

#### Exemplo

[Acesse na OpenAlex](https://openalex.org/works?page=1&filter=default.search%3A%22clarice%20lispector%22%20OR%20%22machado%20de%20assis%22,language%3Alanguages%2Fpt,authorships.countries%3Acountries%2Fbr)
